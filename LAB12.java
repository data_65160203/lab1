public class LAB12 {

    private static void duplicateZeros(int[] arr) {
        int length = arr.length;
        int i = 0;
        int[] temp = new int[length];

        for (int num : arr) {
            if (i == length) {
                break;
            }

            if (num == 0) {
                temp[i++] = 0;
                if (i == length) {
                    break; 
                }
                temp[i++] = 0;
            } else {
                temp[i++] = num;
            }
        }

        for (i = 0; i < length; i++) {
            arr[i] = temp[i];
        }
    }

    public static void main(String[] args) {
        int[] arr1 = {1, 0, 2, 3, 0, 4, 5, 0};
        duplicateZeros(arr1);
        for (int num : arr1) {
            System.out.print(num + " ");
        }
        
        System.out.println();
        
        int[] arr2 = {1,2,3};
        duplicateZeros(arr2);
        for (int num : arr2) {
            System.out.print(num + " ");
        }
    }

}
